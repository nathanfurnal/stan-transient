# Introduction

`stan-transient` provides transient tools to work with Stan, more
precisely with
[cmdstan](https://mc-stan.org/users/interfaces/cmdstan). Interfacing
with the CLI can be somewhat difficult, it's the gap this package will
try to fill. The project is very much in its infancy but I hope it'll
be useful nonetheless.

This package and its functions rely entirely on
[transient](https://github.com/magit/transient).

# Installation

This package is not on MELPA yet. You can either go to the file and
download it, there's an icon to do so in the top right corner. 

You can also [clone the
repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository),
in this you would do :

```
git clone https://gitlab.com/nathanfurnal/stan-transient
```

# Features

- Set your `cmdstan` directory once and forget about it.

- Compile the model in the current `stan-mode`buffer. The path is
  fetched dynamically.
  
- Choose among the different methods and select the data file as well
  as write to the output file without leaving emacs.
  
For now, complex queries and arguments are not supported but the user
can open an asynchronous shell and work from there instead.

# Usage 

I use `use-package` a working configuration is the following, with the
keybindings added. 

``` emacs-lisp
(use-package transient
  :ensure t)
(use-package stan-mode
  :ensure t
  :defer t
  :mode ("\\.stan\\'" . stan-mode)
  :hook (stan-mode . stan-mode-setup)
  :config
  (setq stan-indentation-offset 2)
  (load-file "YOUR/PATH/TO/THIS/FILE/stan-transient.el")
  (setq cmdstan-directory "~/cmdstan")
  :bind (:map stan-mode-map
	      ("C-c C-c" . compile-stan-model)
	      ("C-x s" . stan-transient)))
```

`stan-mode` is loaded on opening of any `.stan` file but `transient`
isn't, as such deferred loading will impede use for now. 

You should set the `cmdstan-directory`somewhere, the default is
`~/cmdstan` but by adding `(setq cmdstan-directory
"my/preferred/path")`it should work as well. 

# Screenshots

![Main stan-transient](screenshots/stan-transient-exemple1.jpg) ![Fit stan-transient](screenshots/stan-transient-exemple2.jpg)

# Roadmap

In the future, I'll plan to add : 

- Running a Stan summary on output files.

- Give more access to the sub-arguments. 

- Allow for the `generate_quantities`argument.

- Meet MELPA standards. 

## Warnings

Since I'm a beginner at ELisp, the functions and the overall package
won't be up to standard, use at your own risk.

### Thanks

To start this project, I relied heavily on [talk about emacs and
Kubernetes](https://www.youtube.com/watch?v=w3krYEeqnyk) as well as
the [accompanying
gist](https://gist.github.com/abrochard/dd610fc4673593b7cbce7a0176d897de).

As well as other packages I shamelessly took inspiration from : 

- [emacs-python-pytest](https://github.com/wbolster/emacs-python-pytest) 
- [sharper.el](https://github.com/sebasmonia/sharper)

### Contributions

Contributions and help are very welcome.
