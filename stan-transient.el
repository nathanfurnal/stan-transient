;;; stan-transient.el --- Transient commands for Stan  -*- lexical-binding: t; -*-

;; Author: Nathan Furnal
;; URL: https://gitlab.com/nathanfurnal/stan-transient
;; Keywords: languages, Stan, emacs.
;; Version: 0.0.1
;; Created: 2020-08-10
;; Package-Requires: ((emacs "26.3") (transient "0.1.0") (stan-mode "10.1.0"))

;;; Commentary:
;; Transient commands for Stan to help completion and parameters
;; input.  For more details see the readme
;; https://gitlab.com/nathanfurnal/stan-transient.

;;; Code:

(require 'transient)
(require 'stan-mode)

;;;========================================
;;; Groups & variables
;;;========================================

;; Useful groups and variable definition
;; The interesting one to set is the stan directory.

(defgroup stan-transient nil
  "Utility group for stan-transient variables."
  :group 'convenience)

(defcustom stan-transient-cmdstan-directory "~/cmdstan/"
  "Path to cmdstan.
This is a probable installation path and the user should set it"
  
  :type 'directory
  :group 'stan-transient)

;;;========================================
;;; Utility functions
;;;========================================

;; Useful functions to improve the parsing of transient's options.

;; There are also Stan related functions to ease getting the relevant
;; paths.


(defun stan-transient--read-file-name-wrapper (prompt initial-input history)
  "Wrapper function to insert PROMPT in a transient friendly way.
Similarly takes INITIAL-INPUT and HISTORY arguments to the same
effect.  Thanks to https://github.com/magit/transient/issues/88"
  
  (read-file-name prompt nil nil t initial-input))

(defun stan-transient--args-to-string (arg-list)
  "Utility function to convert a list of args (ARG-LIST) to a string."
  
  (mapconcat #'identity arg-list " "))

(defun stan-transient--make-formula (model arg-list)
  "Create a single string formula.
The formula takes a string MODEL and a list of args (ARG-LIST)."
  
  (concat model " " (stan-transient--args-to-string arg-list)))
	  
(defun stan-transient--get-model ()
    "Concatenate the name of the directory to get an executable.
Uses the './' symbol and the path's name to dynamically capture the
name of the model."
  (concat "./"
	  (file-name-sans-extension
	   (file-name-nondirectory
	    (buffer-file-name)))))

(defun stan-transient-compile-model ()
  "Compile Stan model in current buffer."

  (interactive)
  (let ((default-directory stan-transient-cmdstan-directory))
    (compile (concat "make -k " (file-name-sans-extension (buffer-file-name))))))

(defun stan-transient-fit-model (&optional args)
  "Fit a Stan model with the given ARGS."
  
  (interactive
   (list (transient-args 'stan-transient-fitting)))
  (let ((default-directory (file-name-directory buffer-file-name))
	(model (stan-transient--get-model)))
    (compile (stan-transient--make-formula model args))))

;;;========================================
;;; Transient arguments
;;;========================================

;; Transient arguments for the "fitting" transient mainly

(transient-define-argument stan-transient-fitting:--methods ()
  :description "Methods choices"
  :class 'transient-option
  :allow-empty nil
  :key "m"
  :argument "method="
  :choices '("sample" "optimize" "variational"))

(transient-define-argument stan-transient-fitting:--input ()
  :description "Input data file"
  :class 'transient-option
  :allow-empty nil
  :shortarg "i"
  :reader 'stan-transient--read-file-name-wrapper
  :prompt "Data file path: "
  :argument "data file=")

(transient-define-argument stan-transient-fitting:--output ()
  :description "Output data file"
  :class 'transient-option
  :allow-empty t
  :shortarg "o"
  :prompt "Output file name: "
  :argument "output file=")

;;;========================================
;;; Transients
;;;========================================

;; Transients prefix proper.

(transient-define-prefix stan-transient ()
  "Interface to send compiled Stan models to the shell."

  ["Compiling"
   ("c" "Compile the current model" stan-transient-compile-model)]
  ["Fitting"
   ("f" "Fit a Stan Model" stan-transient-fitting)]
  ["Asynchronous shell"
   ("s" "Open an async shell" async-shell-command)]
  [""
   ("q" "Quit" transient-quit-one)])

(transient-define-prefix stan-transient-fitting ()
  "Interface to fit a Stan model with parameters."

  ["Methods"
   (stan-transient-fitting:--methods)]
  ["File I/O"
   (stan-transient-fitting:--input)
   (stan-transient-fitting:--output)]
  ["Actions"
   ("f" "Fit the current model" stan-transient-fit-model)]
  [""
   ("q" "Quit" transient-quit-one)
   ("Q" "Quit all" transient-quit-all)])


(provide 'stan-transient)

;;; stan-transient.el ends here
